# OpenML dataset: None

https://www.openml.org/d/45944

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
This dataset "Taxi_Trips_-_2024_20240408.csv" contains information on taxi trips in Chicago starting from February 2024. The dataset includes trip ID, taxi ID, trip start and end timestamps, trip duration in seconds, distance in miles, pickup and dropoff census tracts and community areas, fare amount, tips, tolls, extras, total trip cost, payment type, company, pickup and dropoff centroid latitude and longitude coordinates, and location information.

Attribute Description:
- Trip ID: Unique identifier for each trip
- Taxi ID: Unique identifier for each taxi
- Trip Start Timestamp: Date and time when the trip started
- Trip End Timestamp: Date and time when the trip ended
- Trip Seconds: Duration of the trip in seconds
- Trip Miles: Distance travelled in miles
- Fare: Fare cost of the trip
- Tips: Tip amount
- Tolls: Toll amount
- Extras: Additional charges
- Trip Total: Total cost of the trip
- Payment Type: Payment method used
- Company: Taxi company name
- Pickup and Dropoff Centroid Latitude/Longitude: Coordinates of pickup and dropoff locations
- Pickup and Dropoff Centroid Location: Geographical location of pickup and dropoff points

Use Case:
This dataset can be used to analyze taxi trip patterns, trends, and costs in Chicago, as well as to study customer payment preferences and company performance in the taxi industry. The location data can also be utilized for spatial analysis and mapping of taxi trips in the city.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45944) of an [OpenML dataset](https://www.openml.org/d/45944). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45944/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45944/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45944/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

